from django.urls import include, path
from rest_framework import routers
# from pustakMehal.pustakMehal.api.v0.views.pustak import PustakViewSet
# from api.v0.views import PustakViewSet

from pustak.models import Pustak

from api.views import DummyView
from api.v0.views.pustak import PustakViewSet, PustakDetailsViewSet


router = routers.DefaultRouter()

router.register(r'pustak', PustakViewSet)
router.register(r'pustak-details', PustakDetailsViewSet, basename='pustak-detail')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # path('pustak-details/<int:pk>', pustak_details)
]


