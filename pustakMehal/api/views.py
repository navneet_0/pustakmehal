from django.shortcuts import render
from rest_framework import viewsets

from pustak.models import Pustak

# Create your views here.

class DummyView(viewsets.ModelViewSet):
    queryset = Pustak.objects.all()
