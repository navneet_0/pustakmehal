from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.pagination import PageNumberPagination

from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import mixins, viewsets

from api.v0.serializers.pustak import PustakSerializer, PustakDetailsSerializer

from pustak.models import Pustak, PustakDetails


class PustakViewSet(mixins.ListModelMixin,
                     viewsets.GenericViewSet):
    queryset = Pustak.objects.all()
    serializer_class = PustakSerializer
    # authentication_class = ()
    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        snippets = Pustak.objects.all()
        serializer = PustakSerializer(snippets, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = PustakSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PustakDetailsViewSet(viewsets.ModelViewSet):
    queryset = PustakDetails.objects.all()
    serializer_class = PustakDetailsSerializer
    pagination_class = PageNumberPagination

    def list(self, request, *args, **kwargs):
        id = request.query_params.get('isbn', None)
        if id:
            result_data = self.queryset.filter(pustak__isbn=id)
            serializer = PustakDetailsSerializer(result_data, many=True)
            return Response(serializer.data)


