from rest_framework import serializers

from pustak.models import Pustak, PustakDetails, Category
from api.v0.serializers.author import AuthorSerializer
from api.v0.serializers.publisher import PublisherSerializer


class PustakSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pustak
        fields = '__all__'

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class PustakDetailsSerializer(serializers.ModelSerializer):
    pustak = PustakSerializer()
    author = AuthorSerializer()
    publisher = PublisherSerializer()
    category = CategorySerializer()

    class Meta:
        model = PustakDetails
        # fields = '__all__'
        fields = ['pustak', 'author', 'publisher', 'category', 'price', 'edition']



