from rest_framework import routers

from pustakmehal.pustakMehal.api.v0.views.pustak import PustakViewSet


router = routers.DefaultRouter()

router.register(r'pustak', PustakViewSet, base_name='pustaks')