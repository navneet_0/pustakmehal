from django.db import models

# Create your models here.

class Publisher(models.Model):
    name = models.CharField(max_length=200, null=False, blank=False)
    url = models.URLField(blank=True, null=True)

    def __str__(self):
        return self.name