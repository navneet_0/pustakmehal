from django.db import models

# Create your models here.

from Author.models import Author
from Publisher.models import Publisher

class Pustak(models.Model):
    isbn = models.CharField(max_length=100, unique=True,
            primary_key=True,
            help_text="ISBN is :  International Standard Book Number",)
    title = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title


class Category(models.Model):
    type = models.CharField(max_length=20, unique=True, primary_key=True)
    description = models.CharField(max_length=500, blank=True, null=True)

    def __str__(self):
        return self.type


class PustakDetails(models.Model):
    pustak = models.ForeignKey(Pustak, on_delete=models.CASCADE, related_name='pustak')
    author = models.ForeignKey(Author, on_delete=models.CASCADE, related_name='pustak_author')
    publisher = models.ForeignKey(Publisher, on_delete=models.CASCADE, related_name='pustak_publisher')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='pustak_category')
    price = models.FloatField()
    edition = models.CharField(max_length=20, null=True, blank=True)

    def __str__(self):
        return str(self.pustak)+'-'+str(self.author)

