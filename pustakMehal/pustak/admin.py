from django.contrib import admin
from .models import Pustak, Category, PustakDetails

# Register your models here.

admin.site.register(Pustak)
admin.site.register(Category)
admin.site.register(PustakDetails)