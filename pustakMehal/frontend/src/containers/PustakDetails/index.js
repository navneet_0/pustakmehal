import React from 'react';
import ReactDOM from 'react-dom/client';
import axios from 'axios'; 
import {Table, Spin} from 'antd';
// import { useParams, useNavigate } from 'react-router';
import { useLocation, useParams } from 'react-router-dom';
import _ from 'lodash';


const tableColumns = [

    {
        title: 'ISBN',
        dataIndex: 'isbn',
        key: 'isbn',
        render: (text, record) => {return(<div>{record.isbn}</div>);},
      },

      {
        title: 'Title',
        dataIndex: 'title',
        key: 'title',
        render: (text, record) => {return(<div style={{marginLeft:"21px", fontWeight:"bolder"}}>{record.title}</div>);},
      },
];

// const location = useLocation();
// const { from } = location.isbn;

// const { user_id } = useParams();

class PustakDetails extends React.Component{

    state = {

        bookDetails: []

    };

    //  params = useParams();

    



    async componentDidMount() {
      
        console.log("Mounted ===", this.state, this.props, this.params);
        
        try {
            let isbn = 9780472065219;
          const res = await fetch(`http://127.0.0.1:8000/api/pustak-details/?isbn=${isbn}`);
          const bookDetails = await res.json();
          console.log("=============", bookDetails[0]);
          this.setState({
            bookDetails: bookDetails[0],
          });
        } catch (e) {
          console.log(e);
        }
      }

    render(){



      // let { id } = useParams();
      // console.log("IDDDDDDDD", id);

        console.log(this.state.bookDetails);
        const bookDetails = this.state.bookDetails;
        console.log("--------------", bookDetails);
        if(!_.isEmpty(bookDetails)){
          return(
            // <div style={{margin:"21px"}}>
              <div style={{margin:"auto", width:"60%", padding:"10px"}}>
                <span style={{textAlign:"center"}}><h5><b>Pustak-Details</b></h5></span>
                <h3>{bookDetails.pustak.title}</h3>
                <p><h3>{bookDetails.author.name}</h3></p>
                <p><h3>{bookDetails.category.description}</h3></p>
                <p><h3>{bookDetails.publisher.name}</h3></p>
                <p><h3>{bookDetails.price}</h3></p>'
                
                
            </div>
        );

        }else{
          return <Spin/>;
        }


    };

};


export default PustakDetails;