import React from 'react';
import ReactDOM from 'react-dom/client';
import axios from 'axios'; 
import {Table, Spin} from 'antd';
import {Link} from "react-router-dom";
import { Navigate } from "react-router-dom";


const tableColumns = [

    {
        title: 'ISBN',
        dataIndex: 'isbn',
        key: 'isbn',
        render: (text, record) => {return(<div>{record.isbn}</div>);},
      },

      {
        title: 'Title',
        dataIndex: 'title',
        key: 'title',
        render: (text, record) => {return(<div style={{marginLeft:"21px", fontWeight:"bolder"}}>
          <Link to={`/bookDetails/${record.isbn}`} >
          {record.title}</Link></div>);},
      },
];


class Home extends React.Component{

    state = {

        booksList: []

    };


    async componentDidMount() {
        try {
          const res = await fetch('http://127.0.0.1:8000/api/pustak/');
          const booksList = await res.json();
          this.setState({
            booksList: booksList.results,
          });
        } catch (e) {
          console.log(e);
        }
      }

    render(){

        console.log(this.state.booksList);

        return(
            // <div style={{margin:"21px"}}>
              <div style={{margin:"auto", width:"60%", padding:"10px"}}>
                <span style={{textAlign:"center"}}><h5><b>Pustak-List</b></h5></span>
                <Table
                dataSource={this.state.booksList}
                columns={tableColumns}
                />
                {/* if(this.state.booksList){

                }else{

                } */}
            </div>
        );
    };

};


export default Home;