import logo from './logo.svg';
import './App.css';
import { Route, Switch, Routes, Router } from 'react-router-dom';
import Home from './containers/Home';
import Header from './components/Header';
import  Footer  from './components/Footer';
import AddBook from './components/AddBook';
import PustakDetails from './containers/PustakDetails';
import BookDetails from './components/BookDetails';


function App() {
  return (

    <div style={{marginLeft:"1200px"}}>
    <Router>
      <Route exact path="/" component={Home} />
      <Route path="/addBook" component={AddBook} />
      <Route path="/bookDetails" component={BookDetails}/>
    </Router>

    </div>


  );
}

export default App;
