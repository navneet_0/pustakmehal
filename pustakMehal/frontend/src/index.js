import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import Home from './containers/Home';
import Header from './components/Header';
import  Footer  from './components/Footer';
import AddBook from './components/AddBook';
import BookDetails from './components/BookDetails';
import PustakDetails from './containers/PustakDetails';


import  { BrowserRouter, HashRouter, Router, Route, Routes } from "react-router-dom";


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(


  <BrowserRouter>
  <Header/>
    <Routes>
      <Route path="/" element={<Home />}> </Route>
        <Route path="addBook" element={<AddBook/>}> </Route>
        <Route path='bookDetails/' element={<BookDetails />}>
        <Route path=":isbn" element={<BookDetails />} />
        </Route>
        {/* <Route path='bookDetails/' render={(props) => <PustakDetails myProp="value" {...props}/>}/> */}
        <Route
        path="*"
        element={
        <main style={{ padding: "1rem" }}>
          <p>There's nothing here!</p>
        </main>
      }
    />
    </Routes>
    <Footer/>
  </BrowserRouter>)
  ;

