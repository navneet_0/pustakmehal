import React, { Component } from "react";
import {Button} from 'antd';
import {
    Route,
    NavLink,
    HashRouter
  } from "react-router-dom";

class Footer extends Component {
  render() {
    return (
      <div className="text-center"
      style={{background:"#000000", float:"-moz-initial", marginTop: "80vh"}}>
 
        <hr />
        <Button
        style={{margin:"12px"}}
        type="primary"
        >
            <NavLink to="/">Home</NavLink>
            </Button>

        <Button
        style={{margin:"12px"}}
        type="dashed"
        >
        <NavLink to="/addBook">Add Book</NavLink>
            
        </Button>
      </div>
    );
  }
}

export default Footer;