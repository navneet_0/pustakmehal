import React, { Component } from "react";
import {Divider} from 'antd';

class Header extends Component {
  render() {
    return (
      <div className="text-center">
 
        <hr />
        <h1 style={{textAlign:"center", margin:"10px"}}><i>Pustak-Mehal</i></h1>
        <Divider/>
      </div>
    );
  }
}

export default Header;