import React, { Component , useState} from "react";
import {Button, Form, Input, InputNumber} from 'antd';
import axios from 'axios';

import {center} from '../components/customStyle.css';
import { toHaveFocus } from "@testing-library/jest-dom/dist/matchers";



// const [form] = Form.useForm();
// const [formLayout, setFormLayout] = useState('horizontal');

class AddBook extends Component {

  state = {};

  updateData=(e)=>{
// console.log("Event ",e.target.value, e.target.id);
if(e.target.id === 'isbn'){
  this.setState({
    isbn: e.target.value
  });
} else if(e.target.id === 'title'){
  this.setState({
    title: e.target.value
  });
}

console.log("Current State : ", this.state);
  }



  render() {

    const {isbn, title} = this.state;

    const submitForm = async (e) => { 
      e.preventDefault();
          
      const post = { isbn: isbn,
                    title: title };
  
      try {
        const res = await axios.post('http://localhost:8000/api/pustak/', post)
        console.log(res.data)
      } catch (e) {
        alert(e)
      }
    }

    console.log("Add Render");
    return (
        // <div style={{margin:"100px"}}>

        <div className="center">
<Form
        // {...formItemLayout}
        // labelCol={{
        //   span: 8,
        // }}
        // wrapperCol={{
        //   span: 16,
        // }}
        // form={form}
        // initialValues={{
        //   layout: formLayout,
        // }}
        // onValuesChange={onFormLayoutChange}
        layout="horizontal"
      >
        <Form.Item 
        name='isbn'
        label="ISBN"
        rules={[
          {
            required: true,
            message: "ISBN is a required Field"
          },
          {
            pattern: /^[0-9]+$/,
            message: 'ISBN can have only numbers'
          },
          ({validateFields})=>({
            validator(rules, value) {
              if(value.length !=13){
                return Promise.reject('ISBN has to be 13 digits only')

              }else{
                return Promise.resolve()
              }
            }

          })
        ]}

        onChange={this.updateData}
        >
          <Input
          placeholder="ISBN" 
          
          />
        </Form.Item>

        <Form.Item 
        label="Description">
          <Input
          id="title"
          placeholder="Description" 
          onChange={this.updateData}
          />
        </Form.Item>

        <Form.Item

        style={{margin:"21px"}}
        //  {...buttonItemLayout}
        >
          <Button type="primary"
          onClick={submitForm}>
            Submit</Button>
        </Form.Item>

      </Form>

        </div> 

    );
  }
}

export default AddBook;